import React, { Component } from 'react';
import TodoApp from '../TodoApp';

require('./Layout.scss');

export default class Layout extends Component {

  render() {
    return (
      <div className="layout">
        <TodoApp />
      </div>
    );
  }
}
