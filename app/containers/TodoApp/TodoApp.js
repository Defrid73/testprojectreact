import React, { Component } from 'react';
import Task from '../../components/Task';
import { Icon } from 'react-fa';

import {
  bindToContext, guid, sortByKey,
  getById, setById, removeById,
  saveInLocalStorage, readFromLocalStorage
} from '../../utils';

require('./TodoApp.scss');

export default class TodoApp extends Component {

  constructor(props) {
    super(props);

    this.state = {
      tasks: readFromLocalStorage('todos') || [],
      sortKey: null,
      sortAscending: false
    };

    bindToContext(this, 'onChange', 'onComplete', 'onDelete');
  }

  onChange({id, title, status}) {
    if (!id) {
      id = guid();
      status = 'TODO';
    }

    const tasks = setById(this.state.tasks, id, {
      id,
      title,
      status
    });

    this.setState({ tasks });
    saveInLocalStorage('todos', tasks);
  }

  onComplete(id) {
    const task = getById(this.state.tasks, id);
    const tasks = setById(this.state.tasks, id, {
      ...task,
      status: 'COMPLETE'
    });

    this.setState({ tasks });
    saveInLocalStorage('todos', tasks);
  }

  onDelete(id) {
    const tasks = removeById(this.state.tasks, id);
    this.setState({ tasks });
    saveInLocalStorage('todos', tasks);
  }

  setSorting(sortKey) {
    const newState = {
      sortKey
    };
    newState.sortAscending = this.state.sortKey === sortKey ? !this.state.sortAscending : false;

    this.setState(newState);
  }

  renderSortLink(key, title) {
    let content;
    if (this.state.sortKey === key) {
      const iconName = this.state.sortAscending ? 'sort-asc' : 'sort-desc';

      content = (
        <span>
          {title}
          <Icon name={iconName} />
        </span>
      );
    } else {
      content = title;
    }

    return <a className="header__sort-button" onClick={this.setSorting.bind(this, key)}>{content}</a>;
  }

  renderHeader() {

    return (
      <header className="todo-app__header">
        <span>Sort By:</span>
        {this.renderSortLink('status', 'Status')}
        {this.renderSortLink('title', 'Title')}
      </header>
    );
  }

  renderTasks() {
    const { tasks, sortKey, sortAscending } = this.state;

    if (tasks.length === 0) {
      return null;
    }

    const tasksArray = sortByKey(tasks, sortKey, sortAscending);

    return tasksArray.map((task) => {
      const taskProps = {
        ...task,
        key: task.id,
        onChange: this.onChange,
        onComplete: this.onComplete,
        onDelete: this.onDelete
      };

      return <Task {...taskProps} />;
    });
  }

  renderContent() {
    return (
      <section className="todo-app__content">
        <Task isNew onChange={this.onChange} />
        {this.renderTasks()}
      </section>
    );
  }

  renderFooter() {
    return (
      <footer className="todo-app__footer">
        Total Items: {this.state.tasks.length}
      </footer>
    );
  }

  render() {
    return (
      <div className="todo-app">
        {this.renderHeader()}
        {this.renderContent()}
        {this.renderFooter()}
      </div>
    );
  }
}
