import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';

require('./TitleInput.scss');

export default class TitleInput extends Component {
  static propTypes = {
    onEditFinish: PropTypes.func.isRequired,
    value: PropTypes.string,
    className: PropTypes.string
  };

  getValue() {
    return this.refs.input.value;
  }

  resetValue() {
    this.refs.input.value = this.props.value || '';
  }

  onKeyUp = ({keyCode}) => {
    if (keyCode === 13) {
      this.props.onEditFinish();
    }

    if (keyCode === 27) {
      this.resetValue();
    }
  };

  render() {
    const inputProps = {
      ref: 'input',
      type: 'text',
      className: classNames(this.props.className, 'title-input'),
      defaultValue: this.props.value,
      placeholder: 'What are you going to do?',
      onKeyUp: this.onKeyUp
    };

    return <input {...inputProps} />;
  }
}
