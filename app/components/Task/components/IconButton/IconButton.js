import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import { Icon } from 'react-fa';

require('./IconButton.scss');

export default class IconButton extends Component {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
    iconName: PropTypes.string.isRequired,
    className: PropTypes.string
  };

  render() {
    const className = classNames(this.props.className, 'icon-button');

    return (
      <button onClick={this.props.onClick} className={className}>
        <Icon name={this.props.iconName} size="2x" />
      </button>
    );
  }
}
