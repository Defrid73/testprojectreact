import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import { Icon } from 'react-fa';
import { IconButton, TitleInput } from './components';

import { bindToContext } from '../../utils';

export default class Task extends Component {

  static propTypes = {
    id: PropTypes.string,
    title: PropTypes.string,
    status: PropTypes.string,
    isNew: PropTypes.bool,

    onChange: PropTypes.func.isRequired,
    onComplete: PropTypes.func,
    onDelete: PropTypes.func,
    onAdd: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      isEdit: false
    };

    bindToContext(this, 'onEdit', 'onChange', 'onComplete', 'onDelete');
  }

  onEdit() {
    this.setState({
      isEdit: true
    });
  }

  onChange() {
    const { id, status } = this.props;
    const titleInput = this.refs.titleInput;
    const value = titleInput.getValue();

    if (value === '') {
      return;
    }

    this.props.onChange({
      id,
      status,
      title: value,
    });
    titleInput.resetValue();

    this.setState({
      isEdit: false
    });
  }

  onComplete() {
    if (this.isComplete()) {
      return;
    }

    this.props.onComplete(this.props.id);
  }

  onDelete() {
    if (this.props.isNew) {
      this.refs.titleInput.resetValue();
      return;
    }

    this.props.onDelete(this.props.id);
  }

  isComplete() {
    return this.props.status === 'COMPLETE';
  }

  renderStatus() {
    const className = classNames('task__status', { task__status_complete: this.isComplete()});
    const icon = this.isComplete() ? 'check-circle-o' : 'circle-o';

    return (
      <span className={className}>
        {!this.props.isNew && <Icon name={icon} size="2x" onClick={this.onComplete} />}
      </span>
    );
  }

  renderEdit() {
    const inputProps = {
      ref: 'titleInput',
      className: 'task__title-input',
      value: this.props.title,
      onEditFinish: this.onChange
    };

    return <TitleInput {...inputProps} />;
  }

  renderTitle() {
    if (this.state.isEdit || this.props.isNew) {
      return this.renderEdit();
    }

    const className = classNames('task__title', {
      'task__title_complete': this.isComplete()
    });

    return <label className={className}>{this.props.title}</label>;
  }

  renderButtons() {
    const isEdit = this.props.isNew || this.state.isEdit;
    const editEnabled = !isEdit && !this.isComplete();

    return (
      <div className="task__buttons-container">
        <div className="align-vertical justify-right">
          {isEdit && <IconButton onClick={this.onChange} iconName="check" className="task__button" />}
          {editEnabled && <IconButton onClick={this.onEdit} iconName="pencil" className="task__button" />}
          <IconButton onClick={this.onDelete} iconName="times" className="task__button" />
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="task">
        {this.renderStatus()}
        {this.renderTitle()}
        {this.renderButtons()}
      </div>
    );
  }
}

require('./Task.scss');
