import findIndex from 'lodash/findIndex';
import { find, filter } from 'lodash/collection';

export function bindToContext(context, ...methods) {
  methods.forEach( (method) => context[method] = context[method].bind(context) );
}

export function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return `${s4()}${s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
}

export function sortByKey(array, sortKey, sortAscending) {
  if (!sortKey) {
    return array;
  }

  return array.sort((first, second) => {
    const direction = sortAscending ? -1 : 1;

    if (first[sortKey] > second[sortKey]) {
      return direction;
    }
    if (first[sortKey] < second[sortKey]) {
      return -direction;
    }

    return 0;
  });
}

export function getById(array, id) {
  return find(array, { id });
}

export function setById(array, id, item) {
  const index = findIndex(array, { id });

  if (index === -1) {
    array.push(item);
  } else {
    array[index] = item;
  }

  return array;
}

export function removeById(array, _id) {
  return filter(array, ({ id }) => {
    return id !== _id;
  });
}

export function saveInLocalStorage(key, data) {
  localStorage.setItem(key, JSON.stringify(data));
}

export function readFromLocalStorage(key) {
  return JSON.parse(localStorage.getItem(key));
}
